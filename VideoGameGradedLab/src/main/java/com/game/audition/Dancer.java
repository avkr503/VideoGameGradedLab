package com.game.audition;

import java.util.UUID;

public class Dancer implements Performer {

	private String unionId;
	private String danceStyle;
	
	public Dancer() {
		//this.setUnionId();
	}

	public Dancer(String danceStyle) {
		// TODO Auto-generated constructor stub
		this.setStyle(danceStyle);
	}

	public String getUnionId() {
		// TODO Auto-generated method stub
		return this.unionId;
	}

	public void setUnionId(String id) {
		// TODO Auto-generated method stub
		
		this.unionId = id;
	}

	public String getStyle() {
		// TODO Auto-generated method stub
		return this.danceStyle;
	}

	public void setStyle(String string) {
		// TODO Auto-generated method stub
		this.danceStyle = string;
	}
	
}
