package com.game.audition;

public interface Performer {

	String getUnionId();

	void setUnionId(String id);
	
	void setStyle(String style);
	
	String getStyle();

}
