package com.game.audition;

import java.util.Random;

public class Vocalist implements Performer {

	Random rand = new Random();
	private String unionId;
	private int volume=rand.nextInt(10)+1;
	private char key;
	
	
	public char getKey() {
		return key;
	}

	public void setKey(char key) {
		this.key = key;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}
	@Override
	public String getUnionId() {
		return this.getUnionId();
	}

	@Override
	public void setUnionId(String id) {
		this.unionId= id;
		
	}

	@Override
	public void setStyle(String style) {
		this.setStyle(null);
		
	}

	@Override
	public String getStyle() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Vocalist(String unionId, String key, int volume) {
		this.unionId = unionId;
		this.key = 'g';
		this.volume = volume;
		if(key.length() > 1 || key == null) {
			throw new IllegalArgumentException("The key cannot be either null or cannot have multiple characters");
		}
		if(volume < 1 || volume > 10) {
			throw new IllegalArgumentException("The volume has to be between 1 & 10");
		}
	}

	public Vocalist() {
		// TODO Auto-generated constructor stub
	}
	
	}
