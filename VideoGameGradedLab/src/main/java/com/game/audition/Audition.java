package com.game.audition;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

public class Audition {

	private Performer performer;
	private List<Performer> performerList;
	
	public Audition() {
		this.performerList = new ArrayList<Performer>();
	}
	
	public void addPerformer(String performerType, String id) {
		performer = new Performers();
		performer.setUnionId(id);
		performerList.add(performer);
	}

	public void addPerformer(String performerType, String id, String style) {
		
			performer = new Dancer();
			this.addDancer(id, style);
		
		performerList.add(performer);
	
	}
	
	public void addPerformer(String performerType, String id, String style, boolean singOnVolume) {
		
			performer = new Vocalist();
			this.addVocalist(id, style, singOnVolume);
		
		performerList.add(performer);
	
	}
	
	private void addDancer(String id, String style) {
		// TODO Auto-generated method stub
		this.performer.setUnionId(id);
		this.performer.setStyle(style);
	}

	private void addVocalist(String id, String keyNote, boolean singOnVolume) {
		// TODO Auto-generated method stub
		performer.setUnionId(id);
		performer.setStyle(keyNote);
		Vocalist vocalist = (Vocalist) performer;
		//vocalist.setSingOnVolume(singOnVolume);
		performer = vocalist;
	}
	
	public void printAuditionList() {
		for(Performer performer : this.performerList) {
			if(performer instanceof Vocalist) {
				int singVolume = 0;
				Vocalist vocalist = (Vocalist) performer;
				
					System.out.println("I sing in the key of - "+vocalist.getStyle()+"-"+vocalist.getUnionId());
				}
			}
			if(performer instanceof Dancer) 
			{
				System.out.println(performer.getStyle()+"- "+performer.getUnionId()+" - dancer");
			}
			else {
				System.out.println(performer.getUnionId() + " - Performer");
			}
		}
	

	public List<Performer> getPerformerList() {
		
		return this.performerList;
	}
	
	

}
