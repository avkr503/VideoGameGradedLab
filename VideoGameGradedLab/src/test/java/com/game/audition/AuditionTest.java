package com.game.audition;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.Before;

public class AuditionTest {

	private Audition audition;
	private List<Performer> performerList;
	private List<Performer> actualPerformerList;
	private List<Performer> expectedPerformerList;

	@Before
	public void setUp() {
		audition = new Audition();
		audition.addPerformer("vocalist", "123","G", true);
		audition.addPerformer("dancer", "321", "Salsa");
		audition.addPerformer("dancer", "432", "Tango");
		audition.addPerformer("performer", "234");
		audition.addPerformer("performer", "777");
		audition.addPerformer("performer", "642");
		audition.addPerformer("performer", "981");
		audition.printAuditionList();
		performerList = audition.getPerformerList();
	}
	
	@Test
	public void testGetPerformerList() {
		this.actualPerformerList = audition.getPerformerList();
		this.expectedPerformerList = audition.getPerformerList();
		for(Performer performer : actualPerformerList) {
			System.out.println(performer.getUnionId());
		}
		assertEquals(this.expectedPerformerList, this.actualPerformerList);
	}
	
	
}
