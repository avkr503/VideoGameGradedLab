package com.game.audition;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

public class PerformerTest {

	private Performer performer1;
	private Performer performer2;
	private String expectedIdForVocalist;
	private String actualIdForVocalist;
	private String actualIdForDancer;
	private String expectedIdForDancer;

	@Before
	public void setUp() {
		performer1 = new Vocalist();
		performer2 = new Dancer("Salsa");
	}
	
	@Test
	public void testGetUnionIdVocalist() {
		performer1.setUnionId("111");
		this.expectedIdForVocalist = performer1.getUnionId();
		this.actualIdForVocalist = performer1.getUnionId();
		assertEquals(this.expectedIdForVocalist, this.actualIdForVocalist);
	}
	
	@Test
	public void testGetUnionIdCommuter() {
		performer2.setUnionId("222");
		this.expectedIdForDancer = performer2.getUnionId();
		this.actualIdForDancer = performer2.getUnionId();
		assertEquals(this.expectedIdForDancer, this.actualIdForDancer);
	}
}
