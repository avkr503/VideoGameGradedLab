package com.game.audition;

import static org.junit.Assert.*;
import org.junit.Before;

import org.junit.Test;

public class VocalistTest {

	private Vocalist vocalist;
	private String expectedKey;
	private String actualKey;
	private String actualUnionId;
	private String expectedUnionId;
	private int expectedSingonVolume;
	private boolean actualSingonVolume;

	@Before
	public void setUp() {
		vocalist = new Vocalist();
	}
	
	@Test
	public void testKey() {
		this.expectedKey = "G";
		vocalist.setStyle("G");
		this.actualKey = vocalist.getStyle();
		assertEquals(this.expectedKey, this.actualKey);
	}
	
	@Test
	public void testUnionId() {
		this.actualUnionId = vocalist.getUnionId();
		this.expectedUnionId = vocalist.getUnionId();
		assertEquals(this.expectedUnionId, this.actualUnionId);
	}
	
	@Test
	public void testHasVolume() {
		this.expectedSingonVolume = 5;
		
		assertEquals(this.expectedSingonVolume, this.actualSingonVolume);
	}
}
